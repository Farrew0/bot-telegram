const TelegramApi = require('node-telegram-bot-api')
const {run} = require("nodemon/lib/monitor");
const {rulesToMonitor} = require("nodemon/lib/monitor/match");
const token = '5907967863:AAEYY5qRPeOYbPl7QWPGrRE4Ov1MPQtVJgM'
const bot = new TelegramApi(token, {polling: true})
const {quiet} = require("nodemon/lib/utils");
const timers = require("timers");
const adminUsername = ['cryptokartelI', 'olxxxssd']
const adminChatId = ['5662291645', '1136628065']

const fs = require('fs');

console.log(adminUsername)
let databaseUN = [];
let profiles = {};
let previousStep = '';
let packchoose

function saveDataToFile() {
    const jsonData = { databaseUN, profiles };
    try {
        fs.writeFileSync('data.json', JSON.stringify(jsonData), 'utf8');
        console.log('Data saved successfully.');
    } catch (error) {
        console.error('Error saving data to data.json:', error);
    }
}

function loadData() {
    try {
        const data = fs.readFileSync('data.json', 'utf8');
        const jsonData = JSON.parse(data);
        databaseUN = jsonData.databaseUN || [];
        profiles = jsonData.profiles || {};
    } catch (error) {
        console.error('Error loading data from data.json:', error);
    }
}

function saveData() {
    const jsonData = { databaseUN, profiles };
    try {
        fs.writeFileSync('data.json', JSON.stringify(jsonData), 'utf8');
        console.log('Data saved successfully.');
    } catch (error) {
        console.error('Error saving data to data.json:', error);
    }
}

function createProfile(username) {
    profiles[username] = 0;
    saveData();
}

function getBalance(username) {
    const balance = profiles[username];
    if (balance !== undefined) {
        return balance;
    }
    return '0';
}




loadData();

bot.on('message', (msg) => {
    console.log(msg)
    const text = msg.text;
    const chatId = msg.chat.id;
    const chatUN = msg.chat.username;
    const adminChatUn = msg.from.username

    function sendToAdmins(text) {
        adminChatId.forEach((chatId) => {
            bot.sendMessage(chatId, text);
        });
    }

    if (!databaseUN.includes(chatUN)) {
        databaseUN.push(chatUN);
        createProfile(chatUN);
    }

    if (text === '/start') {
        const keyboard = {
            keyboard: [
                ['© Buy Packs™ ✅'],
                ['💸 Balance 💸', '💲💳 Withdraw 💳💲'],
                ['🛠Options🛠'],
            ],
            resize_keyboard: true,
        };
        const options = {
            reply_markup: JSON.stringify(keyboard),
        };
        return bot.sendMessage(chatId, 'Выберите одну из следующих команд:', options);
    }


        if (chatUN !== adminUsername) {
            bot.setMyCommands([
                {command: '💸 Balance 💸', description: 'Просмотреть баланс'},
                {command: '© Buy Packs™ ✅', description: 'Заказать Пак'},
                {command: '💲💳 Withdraw 💳💲', description: 'Вывод средств'},
                {command: '🛠Options🛠', description: 'Доступные команды'},
            ]);

            if (text === '💸 Balance 💸') {
                const balance = getBalance(chatUN);
                return bot.sendMessage(chatId, `На вашем балансе ${balance}$`);
            }
            if (text === '© Buy Packs™ ✅') {
                previousStep = '© Buy Packs™ ✅';
                const keyboard = {
                    keyboard: [
                        ['🤓 Default | 50$ = (4500₽) 🤓', '💳 Standart | 100$ = (9000₽) 💳', '👔 Clasic | 200$ = (18000₽) 👔'],
                        ['💵 Middle | 400$ = (36000₽) 💵', '💎 Pro | 800$ = (72000₽) 💎', '👑 VIP | 1200$ = (108000₽) 👑'],
                        ['🔙 Go back 🔙'],
                    ],
                    resize_keyboard: true,
                };
                const options = {
                    reply_markup: JSON.stringify(keyboard),
                };
                return bot.sendMessage(chatId, 'Выберите тип пака:', options);
            }
            if (text === '💲💳 Withdraw 💳💲') {
                const inlineKeyboard = {
                    inline_keyboard: [
                        [
                            {text: '📝 Наш менеджер 📝', url: 'https://t.me/cryptokartelI'}
                        ]
                    ]
                };
                const inlineOptions = {
                    reply_markup: JSON.stringify(inlineKeyboard)
                };
                return bot.sendMessage(chatId, 'Если Вы хотите вывести Ваши средства, напишите нашему менеджеру', inlineOptions);
            }
            if (text === '🛠Options🛠') {
                const keyboard = {
                    keyboard: [
                        ['📑Гарантии🔒', '📷 Отзывы 📸',],
                        ['📱 Связь с менеджером 📱'],
                        ['🔙 Go back 🔙'],
                    ],
                    resize_keyboard: true,
                };
                const options = {
                    reply_markup: JSON.stringify(keyboard),
                };
                return bot.sendMessage(chatId, 'Выберите одну из следующих команд:', options);
            }
            if (text === '🔙 Go back 🔙') {
                const keyboard = {
                    keyboard: [
                        ['© Buy Packs™ ✅'],
                        ['💸 Balance 💸', '💲💳 Withdraw 💳💲'],
                        ['🛠Options🛠'],
                    ],
                    resize_keyboard: true
                };
                const options = {
                    reply_markup: JSON.stringify(keyboard)
                };
                return bot.sendMessage(chatId, 'Выберите одну из следующих команд:', options);
            }
            if (text === '📱 Связь с менеджером 📱') {
                const inlineKeyboard = {
                    inline_keyboard: [
                        [
                            {text: '🙋‍♂️ Напишите нам! 🙋‍♂️', url: 'https://t.me/cryptokartelI'},
                        ],
                    ],
                };
                const inlineOptions = {
                    reply_markup: JSON.stringify(inlineKeyboard),
                };
                bot.sendMessage(chatId,
                    `Для связи с менеджером нажмите на кнопку низже. \nМы роботаем 9:00 до 18:00. А для Middle, Pro и VIP 24/7`,
                    inlineOptions
                )
            }
            if (text === '📑Гарантии🔒') {
                const inlineKeyboard = {
                    inline_keyboard: [
                        [
                            {text: 'Наш сайт', url: 'https://cryptokartell-ok8tz.ondigitalocean.app'},
                        ],
                    ],
                };
                const inlineOptions = {
                    reply_markup: JSON.stringify(inlineKeyboard),
                };
                bot.sendMessage(chatId,
                    `Мы Crypto Kartell даем Вам гарантии на такие услуги как:
                
✔ Страховка ваших депозитов

✔ Гарантия безопасности

✔ Гарантия надежности

✔ Гарантия прозрачности

✔ Гарантия поддержки

✔ Гарантия быстрой обработки заявок

За более подробной информации Вы можете перейти на наш сайт.`,
                    inlineOptions
                )
            }
            if (text === '📷 Отзывы 📸') {
                const keyboard = {
                    keyboard: [
                        ['📝 Написать отзыв ✔', '🎞 Просмотреть отзывы 📑'],
                        ['🔙 Go back 🔙']
                    ],
                    resize_keyboard: true
                };
                const options = {
                    reply_markup: JSON.stringify(keyboard)
                };
                return bot.sendMessage(chatId, 'Выберите одну из следующих команд:', options);
            }

            if (text === '🎞 Просмотреть отзывы 📑') {
                const inlineKeyboard = {
                    inline_keyboard: [
                        [
                            {text: '📱 Instagram 📱', url: 'https://www.instagram.com/crypto.kartell/'}
                        ]
                    ]
                };
                const inlineOptions = {
                    reply_markup: JSON.stringify(inlineKeyboard)
                };
                return bot.sendMessage(chatId, 'Если Вы хотите просмотреть отзывы, тогда можете просмотреть наш Instagram профиль и в актуальном Вы сможете просмотреть отзывы людей!', inlineOptions);
            }

            if (text === '📝 Написать отзыв ✔') {
                const inlineKeyboard = {
                    inline_keyboard: [
                        [
                            {text: '📝 Наш менеджер ⏰', url: 'https://t.me/cryptokartelI'}
                        ]
                    ]
                };
                const inlineOptions = {
                    reply_markup: JSON.stringify(inlineKeyboard)
                };
                return bot.sendMessage(chatId, 'Если Вы хотите оставить отзыв, напишите нашему менеджеру', inlineOptions);
            }
            if (text === '🔙 Go back 🔙') {
                const keyboard = {
                    keyboard: [
                        ['📑Гарантии🔒', '📷 Отзывы 📸',],
                        ['📱 Связь с менеджером 📱'],
                        ['🔙 Go back 🔙'],
                    ],
                    resize_keyboard: true
                };
                const options = {
                    reply_markup: JSON.stringify(keyboard)
                };
                return bot.sendMessage(chatId, 'Выберите одну из следующих команд:', options);
            }


            if (text === '🔙 Go back 🔙' && previousStep === '© Buy Packs™ ✅') {
                previousStep = '';
                const keyboard = {
                    keyboard: [
                        ['© Buy Packs™ ✅'],
                        ['💸 Balance 💸', '💲💳 Withdraw 💳💲'],
                        ['🛠Options🛠'],
                    ],
                    resize_keyboard: true,
                };
                const options = {
                    reply_markup: JSON.stringify(keyboard),
                };
                return bot.sendMessage(chatId, 'Выберите одну из следующих команд:', options);
            }

            // Обработка нажатия на клавиши с паками
            if (previousStep === '© Buy Packs™ ✅') {
                switch (text) {
                    case '🤓 Default | 50$ = (4500₽) 🤓':
                    case '💳 Standart | 100$ = (9000₽) 💳':
                    case '👔 Clasic | 200$ = (18000₽) 👔':
                    case '💵 Middle | 400$ = (36000₽) 💵':
                    case '💎 Pro | 800$ = (72000₽) 💎':
                    case '👑 VIP | 1200$ = (108000₽) 👑':
                        packchoose = text
                        const inlineKeyboard = {
                            inline_keyboard: [
                                [
                                    {text: 'Да', callback_data: 'yes'},
                                    {text: 'Нет', callback_data: 'show_keyboard'},
                                ],
                            ],
                        };
                        const inlineOptions = {
                            reply_markup: JSON.stringify(inlineKeyboard),
                        };
                        bot.sendMessage(chatId, `Вы выбрали пакет: ${text}. Вы уверены?`, inlineOptions)
                            .then(() => {
                                const replyMarkup = {
                                    reply_markup: JSON.stringify({remove_keyboard: true}),
                                };
                                bot.sendMessage(chatId, 'Клавиатура с выбором пакетов скрыта.', replyMarkup)
                                    .catch((error) => console.error('Error sending message:', error));
                            })
                            .catch((error) => console.error('Error sending message:', error));
                        break;

                }
            }
        }
    // ADMIN ONLY


    if (adminUsername.includes(chatUN)) {
        bot.setMyCommands([
            {command: '/setbalance', description: 'Установить баланс'},
            {command: '/tracebalance', description: 'Отследить баланс'},
            {command: '/clients', description: 'Клиенты'},
            {command: '/options', description: 'Шаблоны, скрипт и тд'},
        ]);
        const keyboard = {
            keyboard: [
                ['/clients'],
                ['/tracebalance', '/setbalance'],
                ['/options'],
            ],
            resize_keyboard: true,
        };
        const options = {
            reply_markup: JSON.stringify(keyboard),
        };

        if (text.startsWith('/setbalance ')) {
            const commandParts = text.split(' ');
            if (commandParts.length === 3) {
                const targetUsername = commandParts[1];
                const newBalance = parseInt(commandParts[2]);

                if (!isNaN(newBalance)) {
                    profiles[targetUsername] = newBalance;
                    saveData();
                    return bot.sendMessage(
                        chatId,
                        `Баланс пользователя ${targetUsername} установлен на ${newBalance}$`,
                        options
                    );
                } else {
                    return bot.sendMessage(
                        chatId,
                        'Неверный формат баланса. Укажите числовое значение для нового баланса.',
                        options
                    );
                }
            } else {
                return bot.sendMessage(
                    chatId,
                    'Неверный формат команды. Используйте /setbalance <username> <newBalance>',
                    options
                );
            }
        }
        if (text.startsWith('/tracebalance ')) {
            const commandParts = text.split(' ');
            if (commandParts.length === 2) {
                const targetUsername = commandParts[1];
                const balance = getBalance(targetUsername);

                if (balance !== '0') {
                    return bot.sendMessage(
                        chatId,
                        `Баланс пользователя ${targetUsername}: ${balance}$`,
                        options
                    );
                } else {
                    return bot.sendMessage(
                        chatId,
                        `Пользователь ${targetUsername} не найден.`,
                        options
                    );
                }
            } else {
                return bot.sendMessage(
                    chatId,
                    'Неверный формат команды. Используйте /tracebalance <username>',
                    options
                );
            }
        }

        if (text === '/clients') {
            const clients = databaseUN.filter(username => !adminUsername.includes(username));
            if (clients.length > 0) {
                const keyboardButtons = [];
                for (let i = 0; i < clients.length; i += 2) {
                    const client1 = clients[i];
                    const client2 = clients[i + 1];
                    const row = [];
                    row.push({
                        text: client1,
                        callback_data: `client:${client1}`,
                    });
                    if (client2) {
                        row.push({
                            text: client2,
                            callback_data: `client:${client2}`,
                        });
                    }
                    keyboardButtons.push(row);
                }
                const keyboard = {
                    inline_keyboard: keyboardButtons,
                };
                const options = {
                    reply_markup: JSON.stringify(keyboard),
                };
                return bot.sendMessage(chatId, 'Выберите клиента:', options);
            } else {
                return bot.sendMessage(chatId, 'Нет доступных клиентов.');
            }
        }

        if (text === '/options') {
            previousStep = '/start'; // Устанавливаем предыдущий шаг на '/start'
            const keyboard = {
                keyboard: [
                    ['/checkmoney'],
                    ['/script', '/instruction'],
                    ['/back'],
                ],
                resize_keyboard: true,
            };
            const options = {
                reply_markup: JSON.stringify(keyboard),
            };
            return bot.sendMessage(chatId, 'Выберите одну из следующих опций:', options);
        }

        if (text === '/instruction') {

            const inlineKeyboard = {
                inline_keyboard: [
                    [{ text: 'Написать за помощью', url: 'https://t.me/cryptokartelI' }]
                ]
            };

            const options = {
                reply_markup: JSON.stringify(inlineKeyboard)
            }


            bot.sendMessage(chatId, 'Короткая инкструкция как пользыватся ботом :\n' +
                '\n' +
                '1) В пункте /clients Вы можете выбрать клиента,так же у Вас есть выбор между клиентами которые оплатили пак и которые просто пользывались ботом. С любым из пользывавшись ботом клиентов вы можете :\n' +
                '\n' +
                '1.1) Открывать с ним переписку.\n' +
                '\n' +
                '1.2) Установливать ему баланс.\n' +
                '\n' +
                '1.3) Просмотреть его баланс.\n' +
                '\n' +
                '2) В пункте /setbalance Вы можете устанавливать баланс любому из пользывателю бота баланс. Делается очень просто : /setbalance username amount (Пример : /setbalance cryptocartelI 1000)\n' +
                '\n' +
                '3) В пункте /tracebalance Вы можете просматривать баланс любого пользывателя. Делается очень просто : /tracebalance username (Пример : /tracebalance cryptocartelI)\n' +
                '\n' +
                '4) В пункте /options есть 4 подпункта :\n' +
                '\n' +
                '1 | /checkmoney - Данная команда позволит Вам после общение с клиентом проверить, пришли ли деньги на наш кошелёк. Использывать данную команду так же очень просто : /checkmoney | после чего вам бот напишит сообщение "Напишите сумму, которая должна прийти: (написать нужно только число)" после чего Вы вписуете число на которую заказал клиент паков или пака, если Вы ошибились и нажали случайно просто нажмите кнопку /back\n' +
                '\n' +
                '2 | /script - Данная команда упростит Вам общение с клиентом. При нажатии на данную команду Вы сможете просмотреть все часто задаваемые воопросы с ответами. Если находите одинаковый или похожий воопрос то просто копируете ответ и отправляете клиенту.\n' +
                '\n' +
                '3 | /instruction - Данная команда открывает Вам данную инкстукцию.\n' +
                '\n' +
                '4 | /back - Данная команда возращает Вам прошлую меню.\n' +
                '\n' +
                'Как отвечать клиентам :\n' +
                '\n' +
                '1) Соблюдая ВСЕ грамматические нормы и соблюдать субардинацию даже когда Вы его уже обманули.\n' +
                '\n' +
                '2) Если клиент задаёт воопросы то обезательно проверять есть ли данный воопрос в пункте /script.\n' +
                '\n' +
                '3) Если клиент не уверен в покупке Вы можете ему отправлять сами пукты о гарантиях и т.д. на Ваш выбор.\n' +
                '\n' +
                '4) Общение обезательно должно быть на Вы, ТОЛЬКО в случае просьбе клиента перейти на ты Вы можете отвечать клиенту на ТЫ.\n' +
                '\n' +
                'Часто задаваемые воопросы : \n' +
                '\n' +
                '1) Как Вам доказать что перевод от клиента имеено Ваш :\n' +
                '\n' +
                'Что бы небыло недопониманий когда Вы отправляете запрос /checkmoney нам - Админам приходит сообщение с Вашем профилем, временем получением сообщения. А Вы для себя должны записывать каждую сделку которую Вы закрыли.\n' +
                '\n' +
                'Именно для этого команда /checkmoney и создана, что бы небыло обмана между админами (нами) и Вами.\n' +
                '\n' +
                '2) Почему Вам пишет что данный клиент уже занят?\n' +
                '\t\n' +
                'Данное сообщение пишет только втом случае если какой то из Ваших колег уже отвечает данному человеку.\n' +
                '\t\t\n' +
                'Заключение :\n' +
                '\n' +
                'Если у Вас остались воопросы напишите нам в личные сообщение используя кнопку низже.\t \n', options);

        }



        if (text === '/script') {
            bot.sendMessage(chatId, `Вот скрипт: \n
1) А какие гарантии : Мы гарантируем безопасность сделок, заключаемых через нашу платформу. Мы предпринимаем все необходимые меры для обеспечения безопасности ваших финансовых транзакций, включая защиту ваших личных данных и обеспечение безопасности платежей. Мы также производим проверку всех контрагентов, использующих нашу платформу, и отслеживаем их репутацию и историю сделок. В случае возникновения проблем с вашей сделкой, мы готовы обеспечить Вам возврат средств или предоставить другие меры компенсации, так как мы имеем легитимный договор с платформой Binance. Мы ценим наших клиентов и стремимся обеспечить им максимальную защиту при совершении сделок через Binance. \n 

2) Как вы работаете : Суть нашей деятельности - заключается в том, чтобы связать заказчиков с нашими трейдерами/инвесторами. Наша выгода заключается в том что мы берём 20% от результата нашего сотрудничества. \n

2.1) А можно по подробнее : К сожалению данная информация не подлежит разглашению, я как менеджер не имею права этого рассказывать и сам не владею данной информацией. \n

3) У вас есть отзывы : Да, Вы можете найти отзывы наших клиентов на нашей странице в Instagram "crypto.kartell" и на нашем официальном сайте. \n

4) А это легально : Разумеется! Это легально как и для нас, так и для Вас. Наши трейдеры занимаются торговлей на официальной бирже Binance, данный процесс не является противозаконным. \n

5) Сколько времени это занимает : Зачастую это 2-4 дня, в случае задержки, по вашей просьбе, мы можем вернуть Вам полную сумму депозита.  \n

6) Как можно оплатить : У нас доступно 2 способа оплаты - 1: Через надёжный сайт https://onemoment.cc , на котором вы можете оформить депозит с любой карты банка России, 2: Вы можете совершить перевод ваших активов напрямую через Binance. \n

6.1) Можно ли переводом на карту : На данный момент наша компания не принимает оплату с карты на карту. \n

7) Какие есть бонусы для клиентов : Мы даём скидку, за публикацию истории, в которой будет отмечен наш аккаунт, в размере 10% на Pro и 15% на VIP пакеты. \n

8) Какая прибыль мне идёт : В основном у нас выходит приумножить активы клиентов в 3-5 раза.  \n

9) Как я могу отслеживать состояние сделки: Каждый день мы будем оповещать Вас о успешности нашей сделки, мы будем демонстрировать Вам фотографии, на которых будет видно состояние Ваших активов. \n

10) Каким способом вы отправите мне прибыль: Ровно так же, как и было с пополнением. Мы переведём Вам активы одним из 2 способов - 1: Через надёжный сайт https://onemoment.cc, в этом случае деньги придут Вам сразу на банковскую карту. 2: Мы совершим перевод активов напрямую через биржу Binance. По завершению сделки Вы выберете наиболее подходящий Вам вариант. \n

11) Каковы ваши рекомендации по выбору пакета : Мы рекомендуем Вам выбрать Pro или VIP так как на эти пакеты есть одноразовые скидки. Так же, эти пакеты являются наиболее выгодными. \n

12) Какие профессиональные лицензии и сертификаты вы имеете : У нас есть контракт с биржей Binance, который гласит о сотрудничестве и безопасности сделок. \n

13) Каковы риски : Риски минимальны, и даже если они случаются, Вам выплачивают полную сумму депозита и выдают промокод на скидку 10% на любой пакет в знак извинения. \n

13.1) Какая выгода вам покрывать в случае неудачи мой депозит : Мы ценим каждого из наших клиентов и не хотим ставить нашу деятельность и репутацию под сомнения. По этой причине мы возмещаем убытки, случившееся по нашей вине. \n

13.2) Какой шанс на риск : По нашей статистике, всего 2%  на 500 пакетов были не успешны. \n

14) Какие услуги вы предоставляете : Мы предоставляем Вам услугу безопасных инвестиций. Мы - Crypto Kartell инвестируем Ваши деньги, преумножаем их и возвращаем в увеличенном количестве. 
`);


            // Открываем стартовое меню
            const keyboard = {
                keyboard: [
                    ['/checkmoney'],
                    ['/script', '/instruction'],
                    ['/back'],
                ],
                resize_keyboard: true,
            };
            const options = {
                reply_markup: JSON.stringify(keyboard),
            };
            return bot.sendMessage(chatId, 'Выберите одну из следующих опций:', options);
        }
        const requestInfo = {};
        if (text === '/checkmoney') {
            previousStep = '/options'; // Устанавливаем предыдущий шаг на '/options'

            requestInfo[chatId] = {
                userId: chatId,
                requestTime: new Date().toLocaleString()
            };

            const keyboard = {
                keyboard: [
                    ['/back'],
                ],
                resize_keyboard: true,
            };
            const options = {
                reply_markup: JSON.stringify(keyboard),
            };

            // Отправляем сообщение "Напишите сумму, которая должна прийти"
            bot.sendMessage(chatId, 'Напишите сумму, которая должна прийти: (написать нужно только число)', options);
        }

        // Добавляем обработчик текстового сообщения
        if (previousStep === '/options' && !isNaN(parseFloat(text))) {
            const amount = parseFloat(text);

            // Создаем сообщение с информацией о сумме
            const requestTime = new Date().toLocaleString();
            const adminMessage = `Админ ${adminChatUn} запросил просмотр о наличии перевода от клиента в размере ${amount}$, время запроса ${requestTime}`;

            const keyboard = {
                inline_keyboard: [
                    [{text: 'Связатся с ним', url: `https://t.me/${adminChatUn}`}],
                ],
            };

            const options = {
                reply_markup: JSON.stringify(keyboard),
            };

            bot.sendMessage(5662291645, adminMessage, options);
            previousStep = null;
            bot.sendMessage(chatId, 'Администратор скоро напишет Вам в личние сообщения. Ожидайте.');
        }


        if (text === '/back') {
            const keyboard = {
                keyboard: [
                    ['/clients'],
                    ['/tracebalance', '/setbalance'],
                    ['/options'],
                ],
                resize_keyboard: true,
            };
            const options = {
                reply_markup: JSON.stringify(keyboard),
            };
            return bot.sendMessage(chatId, 'Выберите одну из следующих команд:', options);
        }




        //ADMIN OVER



            bot.on('callback_query', (query) => {
                const { message, data } = query;
                const chatId = query.message.chat.id;
                const command = data.split(':')[0];
                const client = data.split(':')[1];
                const messageText = message.text;

                if (command === 'client') {
                    let clientUsername;
                    clientUsername = client;
                    // Обработка выбранного клиента
                    bot.sendMessage(chatId, `Выбран клиент: ${clientUsername}`);

                    // Возвращаем клавиатуру с выбором команд после обработки выбранного клиента
                    const keyboard = {
                        keyboard: [
                            [{ text: '/chat' }],
                            [{ text: '/setbalance' },{ text: '/tracebalance' }],
                            [{ text: '/back' }]
                        ],
                        resize_keyboard: true,
                        one_time_keyboard: true
                    };

                    const options = {
                        reply_markup: JSON.stringify(keyboard)
                    };

                    bot.sendMessage(chatId, 'Выберите команду:', options);
                } else if (command === 'setbalance') {
                    // Обработка команды /setbalance
                    bot.sendMessage(chatId, 'Вы выбрали команду /setbalance');
                } else if (command === 'tracebalance') {
                    // Обработка команды /tracebalance
                    bot.sendMessage(chatId, 'Вы выбрали команду /tracebalance');
                } else if (command === 'chat') {
                    // Обработка команды /chat
                    bot.sendMessage(chatId, 'Вы выбрали команду /chat');
                } else if (command === 'back') {
                    // Обработка команды /back
                    bot.sendMessage(chatId, 'Вы выбрали команду /back');
                } else {
                    // Неподдерживаемая команда
                    bot.sendMessage(chatId, 'Неподдерживаемая команда');
                }


                if (messageText.includes('Вы выбрали пакет')) {
                    if (query.data === 'show_keyboard') {
                        const keyboard = {
                            keyboard: [
                                ['🤓 Default | 50$ = (4500₽) 🤓', '💳 Standart | 100$ = (9000₽) 💳', '👔 Clasic | 200$ = (18000₽) 👔'],
                                ['💵 Middle | 400$ = (36000₽) 💵', '💎 Pro | 800$ = (72000₽) 💎', '👑 VIP | 1200$ = (108000₽) 👑'],
                                ['🔙 Go back 🔙'],
                            ],
                            resize_keyboard: true,
                        };
                        const options = {
                            reply_markup: JSON.stringify(keyboard),
                        };
                        return bot.sendMessage(chatId, 'Выберите тип пака:', options);
                    } else if (query.data === 'yes') {
                        const keyboard = {
                            inline_keyboard: [
                                [{text: 'Купить в пару кликов', callback_data: 'buy_in_clicks'}, {
                                    text: 'Купить через менеджера',
                                    url: 'https://t.me/cryptokartelI'
                                }],
                            ],
                        };
                        const options = {
                            reply_markup: JSON.stringify(keyboard),
                        };
                        return bot.sendMessage(chatId, 'Выберите способ покупки:', options);
                    }
                } else if (query.data === 'yes') {
                    const keyboard = {
                        inline_keyboard: [
                            [{text: 'Купить в пару кликов', callback_data: 'buy_in_clicks'}, {
                                text: 'Купить через менеджера',
                                url: 'https://t.me/cryptokartelI'
                            }],
                        ],
                    };
                    const options = {
                        reply_markup: JSON.stringify(keyboard),
                    };
                    return bot.sendMessage(chatId, 'Выберите способ покупки:', options);
                } else if (query.data === 'buy_in_clicks') {
                    const keyboard = [
                        [
                            { text: 'Перевод на адрес (USDT)', callback_data: 'transfer_usdt' },
                            { text: 'Покупка через Onemoment.cc', callback_data: 'buy_on_website' },
                        ],
                    ];


                    const options = {
                        reply_markup: JSON.stringify({ inline_keyboard: keyboard }),
                    };

                    bot.sendMessage(chatId, 'Выберите способ оплаты:', options)
                        .then((sentMessage) => {
                            // Удаляем предыдущее сообщение
                            deletePreviousMessage(chatId, query.message.message_id);
                        });
                }
                else if (query.data === 'buy_on_website') {
                    const photoUrl = 'https://i.yapx.cc/WFNAw.png';

                    const paymentMessage = `Чтобы оплатить через сайт Onemoment.cc Вам нужно следовать пунктам:
    1) Вам нужно выбрать, каким способом вы будете оплачивать. Их можно просмотреть в пункте "Отдаете".
    2) Вам нужно выбрать USDT Tether TRC20 в пункте "Получаете".
    3) Заполните данные в эту таблицу:
    `;

                    bot.sendMessage(chatId, paymentMessage)
                        .then(() => {
                            bot.sendPhoto(chatId, photoUrl)
                                .then(() => {
                                    // Удаляем предыдущее сообщение
                                    deletePreviousMessage(chatId, query.message.message_id);
                                })
                                .catch((error) => console.error('Ошибка при отправке изображения:', error));
                        })
                        .catch((error) => console.error('Ошибка при отправке сообщения:', error));
                    const address = 'TCVrQeRNpt9X5v1Rte5dhYMNeU62XNHNFS';

                    const keyboard = {
                        inline_keyboard: [
                            [{ text: 'Отправил', callback_data: 'sent' }],
                        ],
                    };

                    const options = {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify(keyboard),
                    };

                    bot.sendMessage(chatId, `Вот адрес USDT (TRC20): \`${address}\`\nНажмите на адрес что бы скопирывать его. `, options)

                }
                else if (query.data === 'transfer_usdt') {
                    const address = 'TCVrQeRNpt9X5v1Rte5dhYMNeU62XNHNFS';

                    const keyboard = {
                        inline_keyboard: [
                            [{ text: 'Отправил', callback_data: 'sent' }],
                        ],
                    };

                    const options = {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify(keyboard),
                    };

                    bot.sendMessage(chatId, `Вот адрес USDT (TRC20): \`${address}\` \nНажмите на адрес что бы скопирывать его.`, options)
                        .then((sentMessage) => {
                            // Удаляем предыдущие сообщения только после нажатия кнопки "Отправил"
                            const messageIdsToDelete = [
                                query.message.message_id,
                                sentMessage.message_id,
                            ];
                            deletePreviousMessage(chatId, query.message.message_id);
                        });
                } else if (query.data.startsWith('copy_address_')) {
                    const address = query.data.split('_')[2];

                    // Отправляем сообщение с адресом
                    bot.sendMessage(chatId, address)
                        .then(() => {
                            // Удаляем предыдущие сообщения
                            deletePreviousMessages(chatId, [query.message.message_id]);
                        })
                        .catch((error) => {
                            console.error('Error sending message:', error);
                        });
                } else if (query.data === 'sent') {
                    // Удаляем предыдущие сообщения
                    deletePreviousMessages(chatId, [query.message.message_id]);

                    // Отправляем сообщение благодарности
                    const message = `Мы проверим вашу транзакцию и течение 5-15 минут Ваши деньги появятся у Вас на балансе, в случае если Вы сделали всё правильно. Если деньги не пришли, обратитесь к нашему менеджеру.`;
                    bot.sendMessage(chatId, message)
                        .then(() => {
                            // Открываем меню после отправки сообщения благодарности
                            const keyboard = {
                                keyboard: [
                                    ['© Buy Packs™ ✅'],
                                    ['💸 Balance 💸', '💲💳 Withdraw 💳💲'],
                                    ['🛠Options🛠'],
                                ],
                                resize_keyboard: true,
                            };
                            const options = {
                                reply_markup: JSON.stringify(keyboard),
                            };
                            return bot.sendMessage(chatId, 'Выберите одну из следующих команд:', options);
                        })
                        .catch((error) => {
                            console.error('Error sending message:', error);
                        });
                    sendToAdmins(`Клиент ${chatUN} возможно оплатил ${packchoose} через бота, зайдите на кошелёк что бы проверить информацию`)
                }
                bot.sendMessage(chatId, message)
                    .then(() => {
                        // Открываем меню после задержки
                        setTimeout(() => {
                            const keyboard = {
                                keyboard: [
                                    ['© Buy Packs™ ✅'],
                                    ['💸 Balance 💸', '💲💳 Withdraw 💳💲'],
                                    ['🛠Options🛠'],
                                ],
                                resize_keyboard: true,
                            };
                            const options = {
                                reply_markup: JSON.stringify(keyboard),
                            };
                            bot.sendMessage(chatId, 'Выберите одну из следующих команд:', options)
                                .catch((error) => {
                                    console.error('Error sending message:', error);
                                });
                        }, 7000); // Задержка в 7 секунд (7000 миллисекунд)
                    })
                    .catch((error) => {
                        console.error('Error sending message:', error);
                    });


            });

// Функция для удаления предыдущего сообщения
        function deletePreviousMessage(chatId, messageId) {
            bot.deleteMessage(chatId, messageId)
                .catch((error) => {
                    console.error('Error deleting message:', error);
                });
        }

// Функция для удаления нескольких предыдущих сообщений
        function deletePreviousMessages(chatId, messageIds) {
            messageIds.forEach((messageId) => {
                deletePreviousMessage(chatId, messageId);
            });
        }
    }
});
